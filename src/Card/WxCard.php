<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2017/12/19
 * Time: 下午5:01
 */

namespace wechatkiy\Card;


class Sku{

    function __construct($quantity){
        $this->quantity = $quantity; //数量
    }

}

/*
微信卡包api SDK V1.0
!!README!!：
base_info的构造函数的参数是必填字段，有set接口的可选字段。
针对某一种卡的必填字段（参照文档）仍然需要手动set（比如团购券Groupon的deal_detail），通过card->get_card()拿到card的实体对象来set。
ToJson就能直接转换为符合规则的json。
Signature是方便生成签名的类，具体用法见示例。
注意填写的参数是int还是string或者bool或者自定义class。
更具体用法见最后示例test，各种细节以最新文档为准。
*/

class DateInfo{
    function __construct($type, $arg0, $arg1 = null)
    {
        if (!is_string($type) )
            // error("DateInfo.type must be integer");
        $this->type = $type;
        if ( $type == 'DATE_TYPE_FIX_TIME_RANGE' )  //固定日期区间
        {
            if (!is_int($arg0) || !is_int($arg1))
                // error("begin_timestamp and  end_timestamp must be integer");
            $this->begin_timestamp = $arg0;
            $this->end_timestamp = $arg1;
        }
        else if ( $type == 'DATE_TYPE_FIX_TERM' )  //固定时长（自领取后多少天内有效）
        {
            if (!is_int($arg0))
                 echo("fixed_term must be integer");
            $this->fixed_term = $arg0;
        }else
            echo("DateInfo.tpye Error");
    }
};
class BaseInfo{

    function __construct($logo_url, $brand_name, $code_type, $title, $color, $notice, $service_phone,
                         $description, $date_info, $sku){

        if(!empty($logo_url))
        $this->logo_url = $logo_url;

        if(!empty($brand_name))
        $this->brand_name = $brand_name;

        if(!empty($code_type))
        $this->code_type = $code_type;

        if(!empty($title))
        $this->title = $title;

        if(!empty($color))
        $this->color = $color;

        if(!empty($notice))
        $this->notice = $notice;

        if(!empty($service_phone))
        $this->service_phone = $service_phone;

        if(!empty($description))
        $this->description = $description;

        if(!empty($date_info))
        $this->date_info = $date_info;

        if(!empty($sku))
        $this->sku = $sku;
    }

    function set_logo_url($logo_url){
        $this->logo_url = $logo_url;
    }

    function set_service_phone($service_phone){
        $this->service_phone = $service_phone;
    }

    function set_use_limit($use_limit){
        if (! is_int($use_limit) )
            // error("use_limit must be integer");
        $this->use_limit = $use_limit;
    }
    function set_get_limit($get_limit){
        if (! is_int($get_limit) )
            // error("get_limit must be integer");
        $this->get_limit = $get_limit;
    }


    function set_can_share($can_share){
        $this->can_share = $can_share;
    }

    /**
     * 卡券是否可转赠。
     * @param $can_give_friend
     */
    function set_can_give_friend($can_give_friend){
        $this->can_give_friend = $can_give_friend;
    }


    /**
     *
     * 是否自定义Code码 。填写true或false，默认为false。 通常自有优惠码系统的开发者选择 自定义Code码，并在卡券投放时带入 Code码，详情见 是否自定义Code码
     *
     * @param $use_custom_code
     */
    function set_use_custom_code($use_custom_code){
        $this->use_custom_code = $use_custom_code;
    }

    /**
     *
     * 是否指定用户领取，填写true或false 。默认为false。通常指定特殊用户群体 投放卡券或防止刷券时选择指定用户领取
     *
     * @param $bind_openid
     */
    function set_bind_openid($bind_openid){
        $this->bind_openid = $bind_openid;
    }

    /**
     * 门店位置poiid。 调用 POI门店管理接 口 获取门店位置poiid。具备线下门店 的商户为必填。
     *
     * @param $location_id_list
     */
    function set_location_id_list($location_id_list){
        $this->location_id_list = $location_id_list;
    }

    /**
     *
     * 设置本卡券支持全部门店，与location_id_list互斥
     * @param $bool
     */
    function set_use_all_locations($bool){
        $this->use_all_locations = $bool;
    }

    /*--------------新版升级，最新字段更新-----------------*/

    /**
     *
     * 卡券顶部居中的按钮，仅在卡券状 态正常(可以核销)时显示
     *
     * @param $center_title
     */
    function set_center_title($center_title){
        $this->center_title = $center_title;
    }

    /**
     *
     * 显示在入口下方的提示语 ，仅在卡券状态正常(可以核销)时显示
     *
     * @param $center_sub_title
     */
    function set_center_sub_title($center_sub_title){
        $this->center_sub_title = $center_sub_title;
    }

    function set_sub_title($sub_title){
        $this->sub_title = $sub_title;
    }

    /**
     * 顶部居中的url ，仅在卡券状态正常(可以核销)时显示
     * @param $center_url
     */
    function set_center_url($center_url){
        $this->center_url = $center_url;
    }

    /**
     *
     * 卡券跳转的小程序的user_name，仅可跳转该 公众号绑定的小程序
     *
     * @param $center_app_brand_user_name
     */
    function set_center_app_brand_user_name($center_app_brand_user_name){
        $this->center_app_brand_user_name = $center_app_brand_user_name;
    }

    /**
     * 卡券跳转的小程序的path
     * @param $center_app_brand_pass
     */
    function set_center_app_brand_pass($center_app_brand_pass){
        $this->center_app_brand_pass = $center_app_brand_pass;
    }

    /**
     * 自定义跳转外链的入口名字。
     * @param $custom_url_name
     */
    function set_custom_url_name($custom_url_name){
        $this->custom_url_name = $custom_url_name;
    }

    /**
     *
     * 自定义跳转的URL
     * @param $custom_url
     */
    function set_custom_url($custom_url){
        $this->custom_url = $custom_url;
    }

    /**
     *
     * 显示在入口右侧的提示语。
     *
     * @param $custom_url_sub_title
     */
    function set_custom_url_sub_title($custom_url_sub_title){
        $this->custom_url_sub_title = $custom_url_sub_title;
    }

    /**
     *
     * 卡券跳转的小程序的user_name，仅可跳转该 公众号绑定的小程序
     * @param $custom_app_brand_user_name
     */
    function set_custom_app_brand_user_name($custom_app_brand_user_name){
        $this->custom_app_brand_user_name = $custom_app_brand_user_name;
    }

    /**
     * 卡券跳转的小程序的path
     * @param $custom_app_brand_pass
     */
    function set_custom_app_brand_pass($custom_app_brand_pass){
        $this->custom_app_brand_pass = $custom_app_brand_pass;
    }

    /**
     * 营销场景的自定义入口名称。
     * @param $promotion_url_name
     */
    function set_promotion_url_name($promotion_url_name){
        $this->promotion_url_name = $promotion_url_name;
    }

    /**
     * 入口跳转外链的地址链接。
     * @param $promotion_url
     */
    function set_promotion_url($promotion_url){
        $this->promotion_url = $promotion_url;
    }

    /**
     * 显示在营销入口右侧的提示语
     * @param $promotion_url_sub_title
     */
    function set_promotion_url_sub_title($promotion_url_sub_title){
        $this->promotion_url_sub_title = $promotion_url_sub_title;
    }

    /**
     * 卡券跳转的小程序的user_name，仅可跳转该 公众号绑定的小程序
     * @param $promotion_app_brand_user_name
     */
    function set_promotion_app_brand_user_name($promotion_app_brand_user_name){
        $this->promotion_app_brand_user_name = $promotion_app_brand_user_name;
    }

    /**
     *
     * 卡券跳转的小程序的path
     *
     * @param $promotion_app_brand_pass
     */
    function set_promotion_app_brand_pass($promotion_app_brand_pass){
        $this->promotion_app_brand_pass = $promotion_app_brand_pass;
    }





};


class AdvancedInfo{

    function set_use_condition($use_condition){
        $this->use_condition = $use_condition;
    }

    function set_abstract($abstract){
        $this->abstract = $abstract;
    }

    function set_text_image_list($text_image_list){
        $this->text_image_list = $text_image_list;
    }
}

/**
 * 高级信息里面的外部的封面摘要结构体名称
 *
 * Class AbstractInfo
 * @package service\func\card
 */
class AbstractInfo{

    function __construct($abstract,$icon_url_list){
        $this->abstract = $abstract;
        $this->icon_url_list = $icon_url_list; //必须是微信侧服的图片地址
    }
}

class TextImage{

    function __construct($image_url,$text){
        $this->image_url = $image_url;
        $this->text = $text;
    }

}

class CardBase{
    public function __construct($base_info,$advanced_info=null){
        $this->base_info = $base_info;
        if($advanced_info){
            $this->advanced_info = $advanced_info;
        }
    }
};

class GeneralCoupon extends CardBase{
    function set_default_detail($default_detail){
        $this->default_detail = $default_detail;
    }
};
class Groupon extends CardBase{
    function set_deal_detail($deal_detail){
        $this->deal_detail = $deal_detail;
    }
};
class Discount extends CardBase{
    function set_discount($discount){
        $this->discount = $discount;
    }
};
class Gift extends CardBase{
    function set_gift($gift){
        $this->gift = $gift;
    }
};
class Cash extends CardBase{
    function set_least_cost($least_cost){
        $this->least_cost = $least_cost;
    }
    function set_reduce_cost($reduce_cost){
        $this->reduce_cost = $reduce_cost;
    }
};
class MemberCard extends CardBase{
    function set_supply_bonus($supply_bonus){
        $this->supply_bonus = $supply_bonus;
    }
    function set_supply_balance($supply_balance){
        $this->supply_balance = $supply_balance;
    }
    function set_bonus_cleared($bonus_cleared){
        $this->bonus_cleared = $bonus_cleared;
    }
    function set_bonus_rules($bonus_rules){
        $this->bonus_rules = $bonus_rules;
    }
    function set_balance_rules($balance_rules){
        $this->balance_rules = $balance_rules;
    }
    function set_prerogative($prerogative){
        $this->prerogative = $prerogative;
    }
    function set_bind_old_card_url($bind_old_card_url){
        $this->bind_old_card_url = $bind_old_card_url;
    }
    function set_activate_url($activate_url){
        $this->activate_url = $activate_url;
    }
};

class ScenicTicket extends CardBase{
    function set_ticket_class($ticket_class){
        $this->ticket_class = $ticket_class;
    }
    function set_guide_url($guide_url){
        $this->guide_url = $guide_url;
    }
};
class MovieTicket extends CardBase{
    function set_detail($detail){
        $this->detail = $detail;
    }
};

class Card{  //工厂
    private	$CARD_TYPE = Array("GENERAL_COUPON",
        "GROUPON", "DISCOUNT",
        "GIFT", "CASH", "MEMBER_CARD",
        "SCENIC_TICKET", "MOVIE_TICKET" );
     const GENERAL_COUPON = 'GENERAL_COUPON';
     const GROUPON = 'GROUPON';
     const DISCOUNT = 'DISCOUNT';
     const GIFT = 'GIFT';
     const CASH = 'CASH';
     const MEMBER_CARD = 'MEMBER_CARD';
     const SCENIC_TICKET = 'SCENIC_TICKET';
     const MOVIE_TICKET = 'MOVIE_TICKET';

    function __construct($card_type, $base_info,$advanced_info=null,$showCardType=true)
    {
        if (!in_array($card_type, $this->CARD_TYPE))
            // error("CardType Error");
        if (! $base_info instanceof BaseInfo )
            // error("base_info Error");
        if($showCardType)
            $this->card_type = $card_type;

        switch ($card_type)
        {
            case $this->CARD_TYPE[0]:
                $this->general_coupon = new GeneralCoupon($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[1]:
                $this->groupon = new Groupon($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[2]:
                $this->discount = new Discount($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[3]:
                $this->gift = new Gift($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[4]:
                $this->cash = new Cash($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[5]:
                $this->member_card = new MemberCard($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[6]:
                $this->scenic_ticket = new ScenicTicket($base_info,$advanced_info);
                break;
            case $this->CARD_TYPE[8]:
                $this->movie_ticket = new MovieTicket($base_info,$advanced_info);
                break;
            default:
                // error("CardType Error");
        }
        return true;
    }
    function get_card()
    {
        switch ($this->card_type)
        {
            case $this->CARD_TYPE[0]:
                return $this->general_coupon;
            case $this->CARD_TYPE[1]:
                return $this->groupon;
            case $this->CARD_TYPE[2]:
                return $this->discount;
            case $this->CARD_TYPE[3]:
                return $this->gift;
            case $this->CARD_TYPE[4]:
                return $this->cash;
            case $this->CARD_TYPE[5]:
                return $this->member_card;
            case $this->CARD_TYPE[6]:
                return $this->scenic_ticket;
            case $this->CARD_TYPE[8]:
                return $this->movie_ticket;
            default:
                // error("GetCard Error");
        }
    }
    function toJson()
    {
        return "{ \"card\":" . urldecode(json_encode($this)) . "}";
    }
};

class Signature{
    function __construct(){
        $this->data = array();
    }
    function add_data($str){
        array_push($this->data, (string)$str);
    }
    function get_signature(){
        sort( $this->data, SORT_STRING );
        return sha1( implode( $this->data ) );
    }
};
