<?php

	namespace wechatkit\Card;

	use wechatkit\Config\Config;
	use wechatkit\Core\Func;
	use wechatkit\HttpFul\HttpFul;

	class Cards
	{
		public function logo($body)
		{
			return HttpFul::init()->handler('uploadimg', [Config::$accessToken], $body, 'JSON', 'FILE');
		}

		public function create($data)
		{
			return HttpFul::init()->handler('card', [Config::$accessToken], $data, 'JSON', 'POST');
		}

		public function apiTicket()
		{
			$CacheDir = sprintf(Config::$config['cachePath'], Config::$options['appid']);
			if (file_exists($CacheDir . DIRECTORY_SEPARATOR . 'CARDJSTICKET')) {
				$fileLastWriteTime = filemtime($CacheDir . DIRECTORY_SEPARATOR . 'CARDJSTICKET');
				if ($fileLastWriteTime && (time() - $fileLastWriteTime < Config::$config['cacheTime'])) { //判断缓存时间
					Config::$CARDJSTICKET = file_get_contents($CacheDir . DIRECTORY_SEPARATOR . 'CARDJSTICKET');
				} else {
					Config::$CARDJSTICKET = $this->ticket($CacheDir);
				}
			} else {
				Config::$CARDJSTICKET = $this->ticket($CacheDir);
			}
		}

		public function ticket($cacheDir)
		{
			$res = HttpFul::init()->handler('getticketCard', [Config::$accessToken], [], 'JSON', 'GET');
			if ($res) {
				file_put_contents($cacheDir . DIRECTORY_SEPARATOR . 'CARDJSTICKET', $res['ticket']);

				return $res['ticket'];
			} else {
				if (function_exists('debug')) {
					debug('card tikcet error');
				}
			}
		}

		public function jsCardConfig($card_id)
		{
			$this->apiTicket();

			$data = [
				'timestamp'  => time(),
				'card_id'    => $card_id,
				'nonce_str'  => Func::nonceStr(32),
				'api_ticket' => Config::$CARDJSTICKET,

			];

			$data['signature'] = $this->sign($data);
			return $data;
		}

		public function sign($data)
		{
			sort($data,SORT_STRING);
			debug('卡券签名：' . json_encode($data, 256));
			$str = '';

			foreach ($data as $val) {
				$str .= $val;
			}
			debug('卡券签名String：' . $str);

			return sha1($str);


		}
	}