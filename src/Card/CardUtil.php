<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2017/12/19
 * Time: 下午7:32
 */

namespace wechatkit\Card;

use wechatkit\HttpFul\HttpFul;
use wechatkiy\Card\Card;
use wechatkit\Core\Common;


class CardUtil{


    /**
     *
     * 返回随机的核销码
     *
     * @return mixed
     *
     */
    static function verifyCode(){

        $verifyCodes = array(
            123,188,
            222,234,
            456,
            555,508,518,520,
            618,666,688,678,
            777,
            888,818,808,
            999,918,998,988
        );

        return $verifyCodes[array_rand($verifyCodes,1)];

    }


    /**
     * 卡券code解密接口
     *
     * 卡券内跳转外链的签名中会对code进行加密处理，通过调用解码接口获取真实code
     * @param $encryptCode
     * @param $accessToken
     * @return null
     */
    static function decrypt($encryptCode,$accessToken){
        $data = array(
            'encrypt_code' =>$encryptCode
        );

        $response = HttpFul::init()->handler(ApiConfig::$decrypt,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return $response['code'];
        }

        return null;
    }


    /**
     *
     * 卡券的LOGO上传到微信图片服务器
     *
     * 先从图片服务器（七牛云）拉取文件到本地，然后再上传至微信服务器,成功后，本地文件会被删除
     *
     * @param $picUrl //图片在图片服务器的地址
     * @param $file //文件在本地的临时存储路径
     *
     * @return null
     */
    static function logo2WxService($picUrl,$accessToken,$file=null){
        if(!$picUrl){
            return null;
        }

        if(!$file)
            $file = APP_ROOT. DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'media'.DIRECTORY_SEPARATOR .mt_rand(0,1000).'.png';

        $location = strpos($picUrl,"qpic.cn");
        if(!$location){
            //拉取文件
            $filePath = Common::httpcopy($picUrl,$file);

            if($filePath){

                //上传文件至微信服务器
                $response = HttpFul::init()->handler('uploadimg', [$accessToken], ['buffer' => $filePath], 'JSON', 'FILE');

                if ($response) {
                    unlink($filePath);
                    $pic_url_wx = $response['url'];
                    return $pic_url_wx;
                } else {
                    echo ('商户logo图标上传微信服务器失败');
                    return null;
                }
            }

        }

        return $picUrl;

    }

    static function createCard($data,$accessToken){

        $response =  HttpFul::init()->handler('card', [$accessToken], $data, 'JSON', 'POST');
        echo('---------'.json_encode($response));

        if($response && ($response['errcode'] == 0)){
            $card_id = $response['card_id'];

            return $card_id;
        }

        return null;

    }


    static function createWxCard($card,$accessToken){
        if(!$card instanceof Card){
         //   error('卡券类型不匹配');
            return false;
        }

        $cardDetail = $card->get_card();

        $baseInfo = $cardDetail->base_info;
        $picUrl = self::logo2WxService($baseInfo->logo_url,$accessToken);
        if(!$picUrl)
            return null;
        $baseInfo->logo_url = $picUrl;

        return self::createCard(['card'=>$card],$accessToken);

    }


    /**
     *
     * 更新卡券基础信息
     *
     * @param $data
     * @param $accessToken
     * @return null
     */
    static function updateWxCard($data,$accessToken){

        $response =  HttpFul::init()->handler(ApiConfig::$update, [$accessToken], $data, 'JSON', 'POST');
        echo('---------'.json_encode($response));

        if($response && ($response['errcode'] == 0)){
            $send_check = $response['send_check'];

            return true;
        }

        return false;
    }


    /**
     *
     * 自主核销
     *
     * @param $cardId
     * @param bool $is_open
     * @param bool $need_verify_code //用户核销时是否需要输入验证码， 填true/false， 默认为false
     * @param bool $need_remark_amount  //用户核销时是否需要备注核销金额， 填true/false， 默认为false
     * @param $accessToken
     * @return bool
     */
    static function selfConsumeCell($cardId,$is_open = true,$need_verify_code = true,$need_remark_amount = true,$accessToken){
        $data = array(
            'card_id' =>$cardId,
            'is_open' =>$is_open,
            'need_verify_code'   =>$need_verify_code,
            'need_remark_amount'  =>$need_remark_amount
        );

        $response = HttpFul::init()->handler(ApiConfig::$selfconsumecell,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;
    }

    /**
     *
     * 修改卡券库存
     *
     * @param $cardId
     * @param $accessToken
     * @param int $increase_stock_value
     * @param int $reduce_stock_value
     * @return bool
     */
    static function modifyStock($cardId,$accessToken,$increase_stock_value=0,$reduce_stock_value=0){

        $data = array(
            'card_id' =>$cardId
        );

        if($increase_stock_value>0){
            $data['increase_stock_value'] = $increase_stock_value;
        }
        if($reduce_stock_value>0){
            $data['reduce_stock_value'] = $reduce_stock_value;
        }


        $response = HttpFul::init()->handler(ApiConfig::$cardModifyStock,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;
    }


    /**
     *
     * 删除卡券（种类）
     *
     * @param $cardId
     * @param $accessToken
     * @return bool
     */
    static function delete($cardId,$accessToken){

        $data = array(
            'card_id' =>$cardId
        );

        $response = HttpFul::init()->handler(ApiConfig::$cardDelete,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;

    }


    /**
     *  使卡券失效
     *
     * @param $cardId
     * @param $code //平台卡券唯一标示
     * @param $accessToken
     * @param string $reason
     * @return bool
     */
    static function unavailable($cardId,$code,$accessToken,$reason=''){
        $data = array(
            'card_id' =>$cardId,
            'code'    =>$code,
            'reason'  => $reason
        );
        $response = HttpFul::init()->handler(ApiConfig::$cardUnAvailable,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;
    }


    /*-----辅助查询能力接口-----*/

    /**
     *
     * 某张卡券当前的信息
     *
     * @param $cardId
     * @param $code
     * @param bool $check_consume
     * @param $accessToken
     * @return bool
     */
    static function codeSelect($cardId,$code,$check_consume=true,$accessToken){
        $data = array(
            'card_id' =>$cardId,
            'code'    =>$code,
            'check_consume'  => $check_consume
        );

        $response = HttpFul::init()->handler(ApiConfig::$cardCodeDetail,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return $response;
        }

        return false;
    }

    /**
     *
     * 卡券API核销
     *
     * @param $code
     * @param $accessToken
     * @param null $cardId
     * @return bool|mixed
     */
    static function consumeCard($code,$accessToken,$cardId= null){
        $data = array(
            'code' => $code
        );
        if($cardId){
            $data['card_id'] = $cardId;
        }

        $response = HttpFul::init()->handler(ApiConfig::$consumeCard,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            return $response;
        }

        return false;
    }

    /**
     *
     * 用于获取用户卡包里的，属于该appid下所有可用卡券，包括正常状态和异常状态
     *
     * @param $openid   //需要查询的用户openid
     * @param null $cardId //卡券ID。不填写时默认查询当前appid下的卡券
     * @param $accessToken
     * @return null
     */
    static function getCardList($openid,$cardId=null,$accessToken){
        $data = array(
            'openid' =>$openid
        );
        if(!empty($cardId)){
            $data['card_id'] = $cardId;
        }

        $response = HttpFul::init()->handler(ApiConfig::$userCardList,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){
            $cardList = $response['card_list'];
            $has_share_card = $response['has_share_card'];
            return $cardList;
        }

        return null;
    }

    /**
     *
     * 查询卡券详情
     *
     * @param $cardId
     * @param $accessToken
     * @return null
     */
    static function cardDetail($cardId,$accessToken){
        $data = array(
            'card_id' =>$cardId
        );

        $response = HttpFul::init()->handler(ApiConfig::$cardDetail,[$accessToken],$data,'JSON','POST');

        if($response && ($response['errcode'] == 0)){

            $cardInfo = $response['card'];
            return $cardInfo;
        }

        return null;
    }


    /**
     *
     * 拉取免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据
     *
     * @param $begin_date //2015-06-15
     * @param $end_date //2015-07-15
     * @param int $cond_source //卡券来源，0为公众平台创建的卡券数据 、1是API创建的卡券数据
     * @param $accessToken
     * @param null $cardId //  如果为null，则拉去总的统计数据
     *
     *
     * 1. 查询时间区间需<=62天，否则报错{errcode: 61501，errmsg: "date range error"}；
     *
     * 2. 传入时间格式需严格参照示例填写”2015-06-15”，否则报错{errcode":61500,"errmsg":"date format error"}
     *
     * 3. 该接口只能拉取非当天的数据，不能拉取当天的卡券数据，否则报错
     *
     * @return null
     */
    static function cardStatistics($begin_date,$end_date,$cond_source=1,$accessToken,$cardId=null){

        $data = array(
            "begin_date" => $begin_date,
            "end_date" => $end_date,
            "cond_source"=> $cond_source
        );

        if($cardId){
            $url = ApiConfig::$certainCardStatistics;
            $data['card_id'] = $cardId;
        }else{
            $url = ApiConfig::$cardStatistics;
        }


        $response = HttpFul::init()->handler($url,[$accessToken],$data,'JSON','POST');

        if($response){

            $dataList = $response['list'];
            return $dataList;
        }

        return null;


    }


}