<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2017/12/19
 * Time: 下午5:49
 */

namespace wechatkit\Card;


class Color{

     const GREEN_LIGHT = 'Color010';      //浅绿色：#63b359
     const GREEN       = 'Color020';      //绿色：#2c9f67
     const BLUE_LIGHT  = 'Color030';      //浅蓝色：#509fc9
     const BLUE        = 'Color040';      //蓝色：#5885cf
     const PURPLE      = 'Color050';      //紫色：#9062c0
     const YELLOW_DARK  = 'Color060';     //深黄色：#d09a45
     const YELLOW_LIGHT = 'Color070';     //浅黄色：#e4b138
     const YELLOW       = 'Color080';     //黄色：#ee903c
     const RED_LIGHT    = 'Color090';     //浅红色：#dd6549
     const RED          = 'Color100';     //红色：#cc463d


    public static function wxColor($name){
        switch ($name){
            case 'green': {
                return self::GREEN;
            }
            case 'yellow':{
                return self::YELLOW;
            }
            case 'red':{
                return self::RED;
            }
            case 'blue':{
                return self::BLUE;
            }
            default :
                return self::GREEN;
        }
    }
}