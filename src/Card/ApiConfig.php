<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2017/12/21
 * Time: 下午3:08
 */

namespace wechatkit\Card;


class ApiConfig{

    public static $cardDetail = 'https://api.weixin.qq.com/card/get?access_token=%1%';

    public static $cardModifyStock = 'https://api.weixin.qq.com/card/modifystock?access_token=%1%';

    public static $cardDelete = 'https://api.weixin.qq.com/card/delete?access_token=%1%';

    public static $cardUnAvailable = 'https://api.weixin.qq.com/card/code/unavailable?access_token=%1%';//使卡券失效，code

    public static $cardCodeUpdate = 'https://api.weixin.qq.com/card/code/update?access_token=%1%';//更改code，在卡券分享时候，用于更换被分享卡券的code值

    public static $selfconsumecell = 'https://api.weixin.qq.com/card/selfconsumecell/set?access_token=%1%';//设置自主核销

    public static $wxCardApiTicket = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%1%&type=wx_card'; //卡券api_ticket

    /**
     * 查询code接口可以查询当前code是否可以被核销并检查code状态。当前可以被定位的状态为正常、已核销、转赠中、已删除、已失效和无效code
     * @var string
     */
    public static $cardCodeDetail = 'https://api.weixin.qq.com/card/code/get?access_token=%1%';

    /**
     *
     * 用于获取用户卡包里的，属于该appid下所有可用卡券，包括正常状态和异常状态
     *
     * @var string
     */
    public static $userCardList = 'https://api.weixin.qq.com/card/user/getcardlist?access_token=%1%';

    /**
     * 拉取本商户的总体数据情况，包括时间区间内的各指标总量。
     * @var string
     */
    public static $cardStatistics = 'https://api.weixin.qq.com/datacube/getcardbizuininfo?access_token=%1%';


    /**
     * 拉取免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据。
     * @var string
     */
    public static $certainCardStatistics = 'https://api.weixin.qq.com/datacube/getcardcardinfo?access_token=%1%';


    /**
     *
     * 卡券消息解密接口
     * @var string
     */
    public static $decrypt = 'https://api.weixin.qq.com/card/code/decrypt?access_token=%1%';

    /**
     *
     * 更新卡券基础信息
     *
     * @var string
     */
    public static $update = 'https://api.weixin.qq.com/card/update?access_token=%1%';

    public static $consumeCard = 'https://api.weixin.qq.com/card/code/consume?access_token=%1%';

}