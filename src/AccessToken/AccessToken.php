<?php
	namespace wechatkit\AccessToken;

	/*
	 * 不完全是 AccessToken 类
	 * 初始化时处理所有的预信息
	 * */
	use wechatkit\Config\Config;
	use wechatkit\Core\ErrorCode;
	use wechatkit\HttpFul\HttpFul;

	class AccessToken{

		static $Certificates = [
			'AccessToken',
			'JsTicket',
		];


        /**
         *
         * 初始化微信令牌参数等
         *
         * @param bool $authSelf
         *              true : 自身维护微信令牌
         *              false: 第三方服务来维护
         *
         */
		static function certificate($authSelf = false){
            if($authSelf){

                self::readCertificates();

            }else{

                $appid = Config::$options['appid'];
                $data = array(
                    'appid' => $appid,
                    'secret' => 'yeran'
                );


                $tokens = HttpFul::init()->handler(Config::$authThirdUrls['accessTokenUrl'],[],$data,'JSON','POST');

                Config::$componentAccessToken = $tokens['componentAccessToken'];
                Config::$accessToken = $tokens['accessToken'];

                Config::$jsApiTicket = null;
                Config::$CARDJSTICKET = null;

            }
        }

		/**
		 * @throws \Exception
		 */
		static function readCertificates(){

			debug('进入初始化微信信息' . PHP_EOL);

			if (Config::$type == 'O') {
				debug('开放平台流程' . PHP_EOL);
				//component_access_token 写入Config
				$componentDir = sprintf(Config::$config['cachePath'], Config::$options['open']['appid']);
				if (!is_dir($componentDir)) {
					if (!mkdir($componentDir, 0777, TRUE)) {
						throw new \Exception('DIR "' . $componentDir . '" no write! place create this');
					}
				}
				if (file_exists($componentDir . DIRECTORY_SEPARATOR . 'component_access_token')) {
					debug('开放平台流程 - component_access_token' . PHP_EOL);
					$fileLastWriteTime = filemtime($componentDir . DIRECTORY_SEPARATOR . 'component_access_token');
					if ($fileLastWriteTime && (time() - $fileLastWriteTime < Config::$config['cacheTime'])) { //判断缓存时间
						debug('开放平台流程 - component_access_token 读取缓存' . PHP_EOL);
						Config::$componentAccessToken = file_get_contents($componentDir . DIRECTORY_SEPARATOR . 'component_access_token');
					} else {
						self::componentAccessToken($componentDir);
					}
				} else {
					self::componentAccessToken($componentDir);
				}
			}

			//加载公众号基本配置
			if (Config::$options['appid']) {
				debug('加载公众号基本配置' . PHP_EOL);
				$CacheDir = sprintf(Config::$config['cachePath'], Config::$options['appid']);
				if (!is_dir($CacheDir)) {
					if (!mkdir($CacheDir, 0777, TRUE)) {
						throw new \Exception('DIR "' . $CacheDir . '" no write! place create this');
					}
				}
				debug('获取参数' . PHP_EOL);

				Config::$accessToken = self::get($CacheDir, 'accessToken', [Config::$options['appid'], Config::$options['secret']]);
				Config::$jsApiTicket = self::get($CacheDir, 'jsApiTicket', [Config::$accessToken]);
			}
		}

        /**
		 *
		 *  根据公众号的appid获取第三方平台对应的accesstoken
		 * 
		 */
		static function getAccessToken($appid){
			$CacheDir = sprintf(Config::$config['cachePath'], $appid);
			debug($CacheDir);
			return self::get($CacheDir, 'accessToken', [$appid, Config::$options['secret']]);
		}

		static function get($CacheDir, $name, $args = [])
		{
			$file = $CacheDir . DIRECTORY_SEPARATOR . $name;
			if (file_exists($file)) {
				$fileLastWriteTime = filemtime($file);
				if ($fileLastWriteTime && (time() - $fileLastWriteTime < Config::$config['cacheTime'])) { //判断缓存时间
					debug('[缓存]获取参数' . $name . '-目录：' . $CacheDir . PHP_EOL);

					return file_get_contents($file);
				} else { //更新缓存
					if (($name == 'accessToken' && Config::$type == 'A') || $name != 'accessToken') {
						$resp = (new HttpFul())->handler($name, $args);
						if ($resp) {
							debug('[微信]获取参数' . $name . '-成功：' . $resp . PHP_EOL);
							Config::$$name = $resp;
							file_put_contents($file, $resp);

							return $resp;
						} else {
							if (function_exists('debug')) {
								debug($name . ' error!');
							}
						}
					} elseif (Config::$type == 'O') {
						$authorizerRefreshToken = file_get_contents($CacheDir . DIRECTORY_SEPARATOR . 'authorizer_refresh_token');
						if ($authorizerRefreshToken) {
							$res = HttpFul::init()->handler('apiAuthorizerToken', [Config::$componentAccessToken], [
								'component_appid'          => Config::$options['open']['appid'],
								'authorizer_appid'         => Config::$options['appid'],
								'authorizer_refresh_token' => $authorizerRefreshToken,
							], 'JSON', 'POST');
							if ($res) {
								debug('[微信][开放]获取参数' . $name . '-成功：' . $res['authorizer_access_token'] . PHP_EOL);
								file_put_contents($file, $res['authorizer_access_token']);
								file_put_contents($CacheDir . DIRECTORY_SEPARATOR . 'authorizer_refresh_token', $res['authorizer_refresh_token']);
								Config::$$name = $res['authorizer_access_token'];
								debug($res['authorizer_access_token']);
								debug(Config::$$name);
							}
						}
					}
				}
			} else { //初次建立
				if (($name == 'accessToken' && Config::$type == 'A') || $name != 'accessToken') {
					$resp = (new HttpFul())->handler($name, $args);
					if ($resp) {
						Config::$$name = $resp;
						file_put_contents($file, $resp);

						return $resp;
					} else {
						if (function_exists('debug')) {
							debug($name . ' error!');
						}
					}
				}
			}
		}

		static protected function componentAccessToken($componentDir)
		{
			debug('开放平台流程 - component_access_token 微信获取'.$componentDir . PHP_EOL);

			if (file_exists($componentDir . DIRECTORY_SEPARATOR . 'component_verify_ticket')) {
				$apiPostData = [
					'component_appid'         => Config::$options['open']['appid'],
					'component_appsecret'     => Config::$options['open']['secret'],
					'component_verify_ticket' => file_get_contents($componentDir . DIRECTORY_SEPARATOR . 'component_verify_ticket'),
				];

				$res = HttpFul::init()->handler('apiComponentToken', [], $apiPostData, 'JSON', 'POST');
				if ($res) {
					debug('开放平台流程 - component_access_token 拉取成功' . PHP_EOL);
					file_put_contents($componentDir . DIRECTORY_SEPARATOR . 'component_access_token', $res['component_access_token']);
					Config::$componentAccessToken = $res['component_access_token'];
				} else {
					if (function_exists('debug')) {
						debug('component_access_token' . ' get fail');
					}

					return FALSE;
				}
			} else {
				if (function_exists('debug')) {
					debug('component_verify_ticket' . '尚未推送');
				}

				return ErrorCode::$componentVerifyTicketNotFound;
			}
		}

	}