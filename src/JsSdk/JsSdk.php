<?php

	namespace wechatkit\JsSdk;


	use wechatkit\Config\Config;
	use wechatkit\Core\Func;

	class JsSdk
	{

		public $jsApiList = [
			'onMenuShareTimeline',
			'onMenuShareAppMessage',
			'onMenuShareQQ',
			'onMenuShareWeibo',
			'onMenuShareQZone',
			'startRecord',
			'stopRecord',
			'onVoiceRecordEnd',
			'playVoice',
			'pauseVoice',
			'stopVoice',
			'onVoicePlayEnd',
			'uploadVoice',
			'downloadVoice',
			'chooseImage',
			'previewImage',
			'uploadImage',
			'downloadImage',
			'translateVoice',
			'getNetworkType',
			'openLocation',
			'getLocation',
			'hideOptionMenu',
			'showOptionMenu',
			'hideMenuItems',
			'showMenuItems',
			'hideAllNonBaseMenuItem',
			'showAllNonBaseMenuItem',
			'closeWindow',
			'scanQRCode',
			'chooseWXPay',
			'openProductSpecificView',
			'addCard',
			'chooseCard',
			'openCard',
		];

		public function jsConfig($url = null, $jsApiList = NULL, $deBug=false)
		{
			if (NULL == $url) {
				$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			}
			if (NULL == $jsApiList) {
				$jsApiList = $this->jsApiList;
			}
			$jsConfig = [
				'debug'     => $deBug,
				'appId'     => Config::$options['appid'],
				'timestamp' => time(),
				'nonceStr'  => Func::nonceStr(32),
			];
			$jsConfig['signature'] = $this->signature($url, $jsConfig);
			$jsConfig['jsApiList'] = $jsApiList;

			return json_encode($jsConfig,256);

		}

		protected function signature($url, $jsConfig)
		{

			$url = explode('#', $url);
			$crrent = $url[0];

			$sign = [
				'noncestr'     => $jsConfig['nonceStr'],
				'jsapi_ticket' => Config::$jsApiTicket,
				'timestamp'    => $jsConfig['timestamp'],
				'url'          => $url[0],
			];

			ksort($sign);

			foreach ($sign as $k => $v) {
				$arr[] = $k . '=' . $v;
			}

			return sha1(join('&', $arr));
		}
	}