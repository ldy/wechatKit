<?php
	namespace wechatkit\Pay;

	use wechatkit\Core\Func;
	use wechatkit\HttpFul\HttpFul;

	class Pay {
		//https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers
		public function promotion($data,$key,$cert_file,$key_file,$ca_file,$type=null){
			
			if($type!=null && 'sendredpack' == $type){
				return $this->sendredpack($data,$key,$cert_file,$key_file,$ca_file);
			}
				
			$data['sign'] = $this->sign($data,$key);
			$xml = $this->arrToXml($data);
			debug('进入企业付款发钱的流程');
			return HttpFul::init()->handler('promotion',[],$xml,'JSON','CA',[
				'cert_file'=>$cert_file,
				'key_file'=>$key_file,
				'ca_file'=>$ca_file,
			]);
		}
		
			/**
		 * 
		 * 公众号红包
		 * 
		 */
		//https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack
		public function sendredpack($data,$key,$cert_file,$key_file,$ca_file){
			debug('进入公众号发钱的流程');
			
			$data['sign'] = $this->sign($data,$key);
			$xml = $this->arrToXml($data);
			
			return HttpFul::init()->handler('sendredpack',[],$xml,'JSON','CA',[
				'cert_file'=>$cert_file,
				'key_file'=>$key_file,
				'ca_file'=>$ca_file,
			]);
		}
		
		

		////			https://api.mch.weixin.qq.com/pay/unifiedorder

		public function unifiedorder($data,$key){
			$data['sign'] = $this->sign($data,$key);
			$xml = $this->arrToXml($data);
			
			$res =  HttpFul::init()->handler('unifiedorder',[],$xml,'XML','POST',[]);
			
			debug(json_encode($res));
			
			if($res['return_code'] == 'SUCCESS' && $res['result_code'] == 'SUCCESS'){
				$payConfig= [
					'appId'=>$data['appid'],
					'timeStamp'=>(string)time(),
					'nonceStr'=>Func::nonceStr(32),
					'package'=>'prepay_id='.$res['prepay_id'],
					'signType'=>'MD5',
					'code_url'=>$res['code_url']
				];

				$payConfig['paySign'] = $this->sign($payConfig,$key);

				return [$res['prepay_id'],$payConfig];
			}else{
				return false;
			}
		}

		protected function sign($data,$key){
			ksort($data);

			foreach ($data as $k=>$v){
				$arr[] = $k.'='.$v;
			}

			$arr[] = 'key='.$key;

			return strtoupper(md5(join('&',$arr)));
		}

		public function arrToXml($data){
			$xml = '<xml>';
			foreach ($data as $k=>$v){
				$xml .= "<{$k}>{$v}</{$k}>";
			}
			$xml .= '</xml>';

			return $xml;
		}
	}