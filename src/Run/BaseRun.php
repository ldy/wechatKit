<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/5/14
 * Time: 下午4:37
 */

namespace wechatkit\Run;


class BaseRun {


    public $options;

    public $authType = 'O';

    public $authorizerAccessToken;
    public $componentAccessToken;

    public function __construct($options)
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getAuthorizerAccessToken()
    {
        return $this->authorizerAccessToken;
    }

    /**
     * @param mixed $authorizerAccessToken
     */
    public function setAuthorizerAccessToken($authorizerAccessToken)
    {
        $this->authorizerAccessToken = $authorizerAccessToken;
    }

    /**
     * @return mixed
     */
    public function getComponentAccessToken()
    {
        return $this->componentAccessToken;
    }

    /**
     * @param mixed $componentAccessToken
     */
    public function setComponentAccessToken($componentAccessToken)
    {
        $this->componentAccessToken = $componentAccessToken;
    }



}