<?php

namespace wechatkit\Run;

use wechatkit\Config, wechatkit\AccessToken;

class Run implements \wechatkit\Core\Run{

    static $self = NULL;
    static $namespace;


    public $options;
    public $cacheConfig;

    /**
     * @param $type string
     * @param array $options [ appid secret mch_id key cert_file key_file ]
     * @param array $config
     * @return $this
     */
    public function App($type, array $options = [], array $config = [],array $authThird = [])
    {

        Config\Config::$type = $type;

        $this->options = array_merge(Config\Config::$options, $options);
        $this->cacheConfig = array_merge(Config\Config::$config, $config);

    //    debug($this->options);
   //     $this->AccessToken()->certificate(false);

        return $this;
    }

    static public function go($class=null)
    {

        if (NULL == self::$self) {
            $instance = new \wechatkit\Run\Run();
            self::$self = $instance;
        } else {
            $instance = self::$self;
        }
        if($class)
            self::$namespace = $class;
        else  self::$namespace = null;


        return $instance;
    }

    public function getAccessToken($appid){

        return $this->AccessToken()->getAccessToken($appid);

    }


    /**
     * @param array $arr
     * @param $value
     * @return array
     */
    protected function key(array $arr, $value)
    {
        return array_keys($arr, $value);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name = null, $arguments)
    {
        $namespace = self::$namespace?:$name;

        if (is_dir(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $namespace)) {
            $className = '\wechatkit\\' . $namespace . '\\' . $name;

            return new $className($this->options);
        } else {
            throw new \Exception('class ' . $name . ' not found!');
        }
    }

    /*
     * O save
     * */
    public function componentVerifyTicketSave($component_verify_ticket)
    {
        $componentDir = sprintf(Config\Config::$config['cachePath'], Config\Config::$options['open']['appid']);
        if (!is_dir($componentDir)) {
            if (!mkdir($componentDir, 0777, TRUE)) {
                throw new \Exception('DIR "' . $componentDir . '" no write! place create this');
            }
        }

        $ret = file_put_contents($componentDir . DIRECTORY_SEPARATOR . 'component_verify_ticket', $component_verify_ticket);
        if ($ret) {
            return TRUE;
        }
    }

    public function certificatesSave($wxappid,$name,$value)
    {
        $componentDir = sprintf(Config\Config::$config['cachePath'], $wxappid);
        if (!is_dir($componentDir)) {
            if (!mkdir($componentDir, 0777, TRUE)) {
                throw new \Exception('DIR "' . $componentDir . '" no write! place create this');
            }
        }

        $ret = file_put_contents($componentDir . DIRECTORY_SEPARATOR . $name, $value);
        if ($ret) {
            return TRUE;
        }
    }


}
