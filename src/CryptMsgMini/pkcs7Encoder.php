﻿<?php

include_once "errorCode.php";

/**
 * PKCS7Encoder class
 *
 * 提供基于PKCS7算法的加解密接口.
 */
class PKCS7Encoder
{
	public static $block_size = 16;

	/**
	 * 对需要加密的明文进行填充补位
	 * @param $text 需要进行填充补位操作的明文
	 * @return 补齐明文字符串
	 */
	function encode( $text )
	{
		$block_size = PKCS7Encoder::$block_size;
		$text_length = strlen( $text );
		//计算需要填充的位数
		$amount_to_pad = PKCS7Encoder::$block_size - ( $text_length % PKCS7Encoder::$block_size );
		if ( $amount_to_pad == 0 ) {
			$amount_to_pad = PKCS7Encoder::block_size;
		}
		//获得补位所用的字符
		$pad_chr = chr( $amount_to_pad );
		$tmp = "";
		for ( $index = 0; $index < $amount_to_pad; $index++ ) {
			$tmp .= $pad_chr;
		}
		return $text . $tmp;
	}

	/**
	 * 对解密后的明文进行补位删除
	 * @param decrypted 解密后的明文
	 * @return 删除填充补位后的明文
	 */
	function decode($text)
	{

		$pad = ord(substr($text, -1));
		if ($pad < 1 || $pad > 32) {
			$pad = 0;
		}
		return substr($text, 0, (strlen($text) - $pad));
	}

}

/**
 * Prpcrypt class
 *
 * 
 */
class Prpcrypt
{
	public $key;

    function __construct($k)
    {
        $this->key = $k;
    }

    /**
     * 加密方法
     * @param string $text
     * @param $appid
     * @return string
     */
    public function  encrypt($text,$appid) {
        try {
            //获得16位随机字符串，填充到明文之前
            $random = $this->getRandomStr();//"aaaabbbbccccdddd";
            $text = $random . pack("N", strlen($text)) . $text . $appid;
            $iv = substr($this->key, 0, 16);
            $pkc_encoder = new PKCS7Encoder;
            $text = $pkc_encoder->encode($text);
            $encrypted = openssl_encrypt($text,'AES-256-CBC',substr($this->key, 0, 32),OPENSSL_ZERO_PADDING,$iv);
            return array(ErrorCode::$OK, $encrypted);
        } catch (Exception $e) {
            //print $e;
            return array(ErrorCode::$EncryptAESError, null);
        }
    }

    /**
     *
     * 解密方法
     * @param $aesCipher
     * @param $aesIV
     * @return array
     *
     */
    public function decrypt($aesCipher,$aesIV) {
        try {

            $decrypted = openssl_decrypt($aesCipher,'AES-128-CBC',$this->key,OPENSSL_RAW_DATA,$aesIV);
            $decrypted = trim($decrypted);
        } catch (Exception $e) {

            return array(ErrorCode::$IllegalBuffer, $e->getMessage());
        }


        try {
            //去除补位字符
            $pkc_encoder = new PKCS7Encoder;
            $result = $pkc_encoder->decode($decrypted);

        } catch (Exception $e) {

            return array(ErrorCode::$IllegalBuffer, null);
        }
        return array(0, $decrypted);

    }
}

?>