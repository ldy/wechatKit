<?php

namespace wechatkit\CryptMsgMini;

include_once "wxBizDataCrypt.php";



class miniDecoder{
	

	public function decoder($appid,$sessionKey,$iv,$encryptedData){
		if(!$sessionKey || !$iv || !$encryptedData){
			return null;
		}
		$data='';
		$pc = new \WXBizDataCrypt($appid, $sessionKey);
		$errCode = $pc->decryptData($encryptedData, $iv, $data);
		
		if ($errCode == 0) {
			
			return $data;
		} else {
		    debug($errCode . "\n");
		}
		
	}

}

?>