<?php

	namespace wechatkit\Service;

	use wechatkit\CryptMsg\CryptMsg;
    use wechatkit\Run\BaseRun;

    class Service extends BaseRun {
		public $postData;
		public $xml;
		public $xmlArray;

		public function handle($postData){

            $this->postData = $postData;//file_get_contents('php://input');
            if(!$this->postData){
               return $this;
            }

            debug('=====================执行微信消息解密流程===================');
            debug($this->postData);

            $cryptmsg = new CryptMsg();
            if($this->authType == 'A'){
                $this->xml = simplexml_load_string($this->postData, 'SimpleXMLElement', LIBXML_NOCDATA);
                if($this->xml->Encrypt){
                    $cryptmsg->WXBizMsgCrypt($this->options['token'],$this->options['encodingAesKey'], $this->options['appid']);
                    $ret = $cryptmsg->decryptMsg($_GET['msg_signature'], $_GET['timestamp'], $_GET['nonce'], $this->postData, $this->xml);
                    $this->xml = simplexml_load_string($this->xml, 'SimpleXMLElement', LIBXML_NOCDATA);
                }
                $this->xmlArray = json_decode(json_encode($this->xml),1);
            }elseif($this->authType == 'O'){
                $cryptmsg->WXBizMsgCrypt($this->options['open']['token'],$this->options['open']['encodingAesKey'], $this->options['open']['appid']);
                $result = $cryptmsg->decryptMsg($_GET['msg_signature'], $_GET['timestamp'], $_GET['nonce'], $this->postData, $this->xml);

                if(0 != $result){
                    #解密失败
                    if(function_exists('debug')){
                        debug('微信数据解密失败'.$result);
                    }
                }else{
                    $this->xml = simplexml_load_string($this->xml, 'SimpleXMLElement', LIBXML_NOCDATA);
                    $this->xmlArray = json_decode(json_encode($this->xml),1);

                    debug("===========微信信息解密成功==========".PHP_EOL.json_encode($this->xmlArray));
                }
            }else{
                $this->xml = simplexml_load_string($this->postData, 'SimpleXMLElement', LIBXML_NOCDATA);
                $this->xmlArray = json_decode(json_encode($this->xml),1);


            }

            return $this;
        }


		public function decryptData(){
			return $this->xml;
		}

		public function decryptArray(){
			return $this->xmlArray;
		}


	}