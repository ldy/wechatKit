<?php

	namespace wechatkit\Core;

	class Func
	{
		static function nonceStr($len)
		{
			$str = array_merge(range(0,9),range('a','z'),range('A','Z'));
			$nonceStr = '';
			for($i=0;$i<$len;$i++){
				$nonceStr .= $str[mt_rand(0,count($str)-1)];
			}

			return $nonceStr;
		}
	}