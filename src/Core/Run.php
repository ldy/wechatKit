<?php
namespace wechatkit\Core;

interface Run{

    /**
     * 初始化
     * @param $type 接入类型
     * @param array $options  当APPID接入必填
     * @return mixed
     */
    public function App($type, array $options=[]);

}

