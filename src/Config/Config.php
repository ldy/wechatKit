<?php

namespace wechatkit\Config;

class Config{

    static public $options = [
                        'appid'=>null,
                        'secret'=>null,
                        'mch_id'=>null,
                        'key'=>null,
                        'cert_file'=>null,
                        'key_file'=>null,
                        'token'=>null,
                        'encodingAesKey'=>null,
                        'accessToken' => '',
                        'open'=>[
                            'appid'=>'',
                            'secret'=>'',
                            'token'=>null,
                            'encodingAesKey'=>null,
                            'componentAccessToken'=>''
                        ],
                        'webLogin'=>[
                            'appid'=>'',
                            'secret'=>''
                        ]
                    ];
    static public $type = 'O';
    static public $config = [
        'cachePath' => APP_ROOT.'/cache/file/%s', //%s 为APPID替代 此公众号下所有凭证都会缓存在此目录下
        'cacheTime' => '7000', //缓存有效时间
    ];
    /**
     *
     * 第三方授权维护服务的链接
     *
     * @var array
     */
    static public $authThirdUrls = array(
        'accessTokenUrl' => null,
        'jsApiTicketUrl' => null,
        'cardJsTicketUrl' => null
    );

    static public $authSelf = false;    //是否本平台维护授权，默认为false

    static public $typeList = [
                    'openHandler'=>'O',            //开放平台接入
                    'mpHandler'=>'A'             //普通接入
                    ];


    static public $jsApiTicket;
    static public $oAuthAccessToken;
    static public $CARDJSTICKET;
 }