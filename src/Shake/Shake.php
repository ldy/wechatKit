<?php

	namespace wechatkit\Shake;

	use wechatkit\Config\Config;
	use wechatkit\HttpFul\HttpFul;

	class Shake {
		public function addPage($data){
			return HttpFul::init()->handler('pageAdd',[Config::$accessToken],$data,'JSON','POST');
		}

		public function updatePage($data){
			
			return HttpFul::init()->handler('pageUpdate',[Config::$accessToken],$data,'JSON','POST');
		}

		public function uploads($data,$type){
			return HttpFul::init()->handler('shakearoundMaterialAdd',[Config::$accessToken,$type],$data,'JSON', 'FILE');
		}

		public function getShakeInfo($data){
			return HttpFul::init()->handler('getshakeinfo',[Config::$accessToken],$data,'JSON','POST');
		}
		

	}