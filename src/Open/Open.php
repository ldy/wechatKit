<?php

	namespace wechatkit\Open;

	use Lestore\Wxauthcenter\Services\AccessTokenService;
    use wechatkit\Config\Config;
	use wechatkit\HttpFul\HttpFul;
    use wechatkit\Run\BaseRun;

    class Open extends BaseRun
	{

		//网页登入
		public function webOAuth2($url = NULL, $type = 'URL', $state = NULL, $wxLoginConfig = ['style' => 'black',
																							   'href'  => NULL])
		{
            $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';

			if(null == $url){
				$url = $http_type.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			}

			if ('URL' == $type) {
				$locationUrl = 'https://open.weixin.qq.com/connect/qrconnect?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_login&state=%s#wechat_redirect';
				header('Location:' . sprintf($locationUrl, Config::$options['webLogin']['appid'], urlencode($url), $state));
				exit(255);
			} elseif ('JSON' == $type) {
				//<script src="http://res.wx.qq.com/connect/zh_CN/htmledition/js/wxLogin.js"></script>
				$wxLogin = [
					'id'           => 'wxLogin',
					'appid'        => Config::$options['webLogin']['appid'],
					'scope'        => 'snsapi_login',
					'redirect_uri' => $url,
					'state'        => $state,
					'style'        => $wxLoginConfig['style'],
					'href'         => $wxLoginConfig['href'],
				];

				return json_encode($wxLogin, 256);
			}
		}

		public function codeAccessToken($code)
		{
//			public $oauth2AccessToken = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%1%&secret=%2%&code=%3%&grant_type=authorization_code ';

			$httpFul = new HttpFul();

			return $httpFul->handler('oauth2AccessToken',
				[
					Config::$options['webLogin']['appid'],
					Config::$options['webLogin']['secret'],
					$code,
				]);
		}

		public function userInfo($accessToken, $openid)
		{
			return HttpFul::init()->handler('oAuth2AccessTokenUserInfo', [$accessToken, $openid]);
		}


        /**
         * 微信公众号/小程序授权
         * @param $authType 1则商户扫码后，手机端仅展示公众号、2表示仅展示小程序，3表示公众号和小程序都展示,如果为未指定，则默认小程序和公众号都展示
         * @param null $url
         * @return bool|mixed
         */
		public function authorizer($authType=3,$url=null){

			if(!array_key_exists('auth_code',$_GET) ||  !$_GET['auth_code']){

			    $componentAppid = $this->options['open']['appid'];
                $componentToken = AccessTokenService::componentAccessToken($this->options);
                if(empty($componentToken)){
                    return false;
                }
				$preAuthCode = HttpFul::init()->handler('preAuthCode'
                    ,[$componentToken]
                    ,['component_appid'=>$componentAppid],'JSON','POST');

				if($preAuthCode){
					if(null == $url){
                          $url = httpType().$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					}
                    $url = sprintf('Location:https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=%s&pre_auth_code=%s&redirect_uri=%s'
                        ,$componentAppid,$preAuthCode['pre_auth_code'],urlencode($url));
                    $url .= '&auth_type='.$authType;
                    header($url);
                    exit(255);
				}
				return false;
			}else{
				return $this->authinfo($_GET['auth_code']);
			}
		}


        /**
         * 拉公众号详细信息
         *
         * @param $authorizer_appid
         * @return bool|mixed
         */
		public function mpInfo($authorizer_appid){

		    return HttpFul::init()->handler('apiGetAuthorizerInfo',[AccessTokenService::componentAccessToken($this->options)]
                ,['component_appid'=>$this->options['open']['appid'],'authorizer_appid'=>$authorizer_appid],'JSON','POST');

		}

        /**
         * 获取公众号授权信息
         * @param $auth_code
         * @return bool|mixed
         */
		public function authinfo($auth_code){
			return HttpFul::init()->handler('apiQueryAuth',[AccessTokenService::componentAccessToken($this->options)]
                ,['component_appid'=>$this->options['open']['appid'],'authorization_code'=>$auth_code],'JSON','POST');
		}
	}