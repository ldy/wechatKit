<?php

	namespace wechatkit\Device;

	use wechatkit\Config\Config;
	use wechatkit\HttpFul\HttpFul;

	class Device
	{
		public function applyid($data)
		{
			//https://api.weixin.qq.com/shakearound/device/applyid?access_token=ACCESS_TOKENPOST
			return HttpFul::init()->handler('applyid',[Config::$accessToken],$data,'JSON','POST');
		}

		public function applystatus($data){
//			https://api.weixin.qq.com/shakearound/device/applystatus?access_token=ACCESS_TOKEN
			return HttpFul::init()->handler('applystatus',[Config::$accessToken],$data,'JSON','POST');
		}

		public function search($data){
//			https://api.weixin.qq.com/shakearound/device/search?access_token=ACCESS_TOKENPOST
			return HttpFul::init()->handler('deviceSearch',[Config::$accessToken],$data,'JSON','POST');

		}

		public function addPage($data){
//			https://api.weixin.qq.com/shakearound/device/bindpage?access_token=ACCESS_TOK
			return HttpFul::init()->handler('deviceeBindpage',[Config::$accessToken],$data,'JSON','POST');
		}
	}