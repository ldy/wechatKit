<?php

	namespace wechatkit\Oauth2;

	use Lestore\Wxauthcenter\Services\AccessTokenService;
    use wechatkit\Config\Config;
	use wechatkit\HttpFul\HttpFul;
    use wechatkit\Run\BaseRun;

    class Oauth2 extends BaseRun
	{
		
		/**
		 * 
		 * 微信小程序登陆
		 * 
		 * @param $appid
		 * @param $secret
		 * 
		 */
		public function jscode2session($appid,$secret,$code){
			if ($code){
				$result = $this->accessTokenFromJsCode($code,$appid,$secret, new HttpFul());
				if($result)
				return $result;
				else 
					return null;

			}
			return null;
		}
		
		/**
		 * 
		 * 通过微信小程序code获取用户openid
		 * 
		 * @param code
		 * @param HttpFul $httpFul
		 * 
		 * 
		 */
		protected function accessTokenFromJsCode($code,$appid,$secret,HttpFul $httpFul){
			if(!$code)
				return null;
			$result = $httpFul->handler('openOauth2JSAccessToken', [$appid,$secret,$code]);
			

			return $result;
		}

		/**
         * 公众号授权
         *
		 * @param $redirect_uri
		 * @param string $scope
		 * @param string $state
		 * @param bool $auto
		 * @return bool|mixed
		 */
		public function mpAuthorize($redirect_uri =null, $scope = 'snsapi_userinfo', $state = '', $auto = TRUE)
		{

			if(null == $redirect_uri){
				$redirect_uri = httpType().$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			}

			if ($this->authType == 'O') { //开放平台授权
				if (!isset($_GET['code']) || !$_GET['code']) {
				    $url = sprintf(
                        'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s&component_appid=%s&connect_redirect=1#wechat_redirect',
                        $this->options['appid'],
                        urlencode($redirect_uri),
                        $scope,
                        $state,
                        $this->options['open']['appid']
                    );
					header('location:' .$url,TRUE,301);
					exit(255);
				} else {
					if ($auto) {
						$result = $this->accessTokenFormCode($_GET['code']);
						if(empty($result)){
						    return null;
                        }
						if ('snsapi_base' == $scope) {
						    debug($result['openid']);
							return ['openid' => $result['openid']];
						}

						$info = $this->oAuth2AccessTokenUserInfo($result['openid'],$result['access_token']);

						return $info;
					}
				}
			}
            return null;
		}


        /**
         *
         * 通过code获取accessToken
         *
         * @param $code
         * @return bool|mixed
         */
		protected function accessTokenFormCode($code)
		{
            $result = null;
			if($this->authType == 'O') {
                $componentAccessToken = AccessTokenService::componentAccessToken($this->options);

                if (!empty($componentAccessToken)) {
                    $result = HttpFul::init()->handler('openOauth2AccessToken'
                        , [$this->options['appid'], $code, $this->options['open']['appid'], $componentAccessToken]);
                }
            }
			return $result;
		}

        /**
         * @param $openid
         * @param $accessToken   //登录使用的accesstoken
         * @return bool|mixed
         */
		protected function oAuth2AccessTokenUserInfo($openid,$accessToken)
		{
			$result = HttpFul::init()->handler('oAuth2AccessTokenUserInfo', [$accessToken, $openid]);

			return $result;
		}


	}
