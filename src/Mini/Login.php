<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/1/9
 * Time: 下午2:53
 */

namespace wechatkit\Mini;

use Lestore\Wxauthcenter\Services\AccessTokenService;
use wechatkit\HttpFul\HttpFul;
use wechatkit\Run\BaseRun;

/**
 *
 * 微信授权登陆
 *
 * Class Login
 * @package wechatkit\Mini
 */
class Login  extends BaseRun{


    public function mpAuthorize($jsCode){

       return $this->jscode2session($jsCode);
    }


    /**
     *
     * 微信小程序登陆
     *
     * @param $jsCode
     * @return bool|mixed|null
     * @internal param $appid
     * @internal param $secret
     */
    public function jscode2session($jsCode){
        if ($jsCode){
            $appid = $this->options['appid'];
            $componentAppid = $this->options['open']['appid'];
            $componentAccessToken = AccessTokenService::componentAccessToken($this->options);

            $result = null;
            if($componentAccessToken) {
                $result = $this->accessTokenFromJsCode($jsCode, $appid, $componentAppid, $componentAccessToken);
            }

            return $result;
        }
        return null;
    }

    /**
     *
     * 通过微信小程序code获取用户openid
     *
     * @param $jsCode
     * @param $appid
     * @param $componentAppid
     * @param $componentAccessToken
     * @return bool|mixed|null
     * @internal param $code
     * @internal param HttpFul $httpFul
     *
     */
    protected function accessTokenFromJsCode($jsCode,$appid,$componentAppid,$componentAccessToken){
        if(!$jsCode)
            return null;
        $result = HttpFul::init()->handler('thirdOauth2JsAccessToken', [$appid,$jsCode,$componentAppid,$componentAccessToken]);


        return $result;
    }

}
