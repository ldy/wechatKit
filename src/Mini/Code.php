<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/1/9
 * Time: 下午2:21
 */

namespace wechatkit\Mini;

use wechatkit\Config\Config;
use wechatkit\HttpFul\HttpFul;


/**
 *
 * 代码管理
 *
 * Class Code
 * @package wechatkit\Mini
 *
 */
class Code {


    /**
     *
     * 为授权的小程序帐号上传小程序代码
     *
     * @param $template_id //代码库中的代码模版ID
     * @param $ext_json //第三方自定义的配置，如果代码中已经有配置，则配置的合并规则为：除了pages和tabBar.list直接覆盖原配置，其他都为插入或同级覆盖
     * @param $user_version //代码版本号，开发者可自定义
     * @param $user_desc //代码描述，开发者可自定义
     * @param null $accessToken
     *
     * @return bool
     */
    public function commitCode($template_id,$ext_json,$user_version='v1.0.0',$user_desc='第一次发布版本',$accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array(
            'template_id'    => $template_id,
            'ext_json'  =>$ext_json,
            'user_version'  =>$user_version,
            'user_desc' => $user_desc
        );

        $response =  HttpFul::init()->handler(ApiConfig::$commit, [$accessToken], $data, 'JSON', 'POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;

    }

    /**
     *
     * 获取体验小程序的体验二维码
     *
     * @param $accessToken
     * @return bool|mixed
     */
    public function getQrcode($accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $response =  HttpFul::init()->handler(ApiConfig::$commit, [$accessToken], [], null, 'GET');

        if(is_array($response) && $response['errcode'] == -1){
            return false;
        }else{
            return $response;
        }
    }

    /**
     * 将第三方提交的代码包提交审核（仅供第三方开发者代小程序调用）
     *
     * @param $item_list
     * @param null $accessToken
     * @return bool|mixed
     */
    public function submitAudit($item_list,$accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array(
            'item_list' => $item_list
        );

        $response =  HttpFul::init()->handler(ApiConfig::$submit_audit, [$accessToken], $data, 'JSON', 'POST');

        return $response;
    }


    /**
     *
     * 发布已通过审核的小程序（仅供第三方代小程序调用）
     *
     * @param null $accessToken
     * @return bool|mixed
     */
    public function release($accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array();

        $response =  HttpFul::init()->handler(ApiConfig::$release, [$accessToken], $data, 'JSON', 'POST');

        return $response;

    }

    /**
     * 设置小程序是否用户可见，刚发布之后的小程序是默认为可见的，因此这里的默认值是不可见
     * @param string $action
     * @param null $accessToken
     * @return bool|mixed
     */
    public function changeVisitstatus($action='close',$accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array(
            'action'    =>$action
        );

        $response =  HttpFul::init()->handler(ApiConfig::$change_visitstatus, [$accessToken], $data, 'JSON', 'POST');

        return $response;
    }


    /**
     * 小程序版本回退（仅供第三方代小程序调用）
     *
     * @param null $accessToken
     * @return bool|mixed
     */
    public function revertCodeRelease($accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $response =  HttpFul::init()->handler(ApiConfig::$revertcoderelease, [$accessToken], null, null, 'GET');

        return $response;
    }



}