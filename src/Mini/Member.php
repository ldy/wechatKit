<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/1/9
 * Time: 下午2:06
 */

namespace wechatkit\Mini;

use wechatkit\Config\Config;
use wechatkit\HttpFul\HttpFul;

/**
 *
 * 小程序成员管理
 *
 * Class Member
 * @package wechatkit\Mini
 */
class Member {

    public function bindTester($wechatid,$accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array(
            'wechatid'    => $wechatid
        );

        $response =  HttpFul::init()->handler(ApiConfig::$bindTester, [$accessToken], $data, 'JSON', 'POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;

    }


    public function unbindTester($wechatid,$accessToken=null){


        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array(
            'wechatid'    => $wechatid
        );

        $response =  HttpFul::init()->handler(ApiConfig::$unbindTester, [$accessToken], $data, 'JSON', 'POST');

        if($response && ($response['errcode'] == 0)){

            return true;
        }

        return false;

    }

}