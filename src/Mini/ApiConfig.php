<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/1/9
 * Time: 下午1:37
 */

namespace wechatkit\Mini;


class ApiConfig{

    /**
     *
     * 需要先将域名登记到第三方平台的小程序服务器域名中，才可以调用接口进行配置。
     *
     * @var string
     */
    public static $modifyDomain = 'https://api.weixin.qq.com/wxa/modify_domain?access_token=%1%';


    /**
     *
     * 授权给第三方的小程序，其业务域名只可以为第三方的服务器，当小程序通过第三方发布代码上线后，小程序原先自己配置的业务域名将被删除，只保留第三方平台的域名，所以第三方平台在代替小程序发布代码之前，需要调用接口为小程序添加业务域名。
     *   提示：需要先将域名登记到第三方平台的小程序业务域名中，才可以调用接口进行配置
     *
     * @var string
     *
     */
    public static $setwebviewDomain = 'https://api.weixin.qq.com/wxa/setwebviewdomain?access_token=%1%';


   /*---成员管理---*/

    public static $bindTester = 'https://api.weixin.qq.com/wxa/bind_tester?access_token=%1%';

    public static $unbindTester = 'https://api.weixin.qq.com/wxa/unbind_tester?access_token=%1%';



    /*---代码管理----*/

    /**
     * 为授权的小程序帐号上传小程序代码
     * @var string
     */
    public static $commit = 'https://api.weixin.qq.com/wxa/commit?access_token=%1%';

    /**
     * 获取体验小程序的体验二维码
     * @var string
     */
    public static $get_qrcode = 'https://api.weixin.qq.com/wxa/get_qrcode?access_token=%1%';

    /**
     *
     * 获取授权小程序帐号的可选类目
     *
     * @var string
     */
    public static $get_category = 'https://api.weixin.qq.com/wxa/get_category?access_token=%1%';

    /**
     * 获取小程序的第三方提交代码的页面配置（仅供第三方开发者代小程序调用)
     * @var string
     */
    public static $get_page = 'https://api.weixin.qq.com/wxa/get_page?access_token=%1%';

    /**
     * 将第三方提交的代码包提交审核（仅供第三方开发者代小程序调用）
     *
     * @var string
     */
    public static $submit_audit = 'https://api.weixin.qq.com/wxa/submit_audit?access_token=%1%';


    /**
     * 查询某个指定版本的审核状态（仅供第三方代小程序调用)
     * @var string
     */
    public static $get_auditstatus = 'https://api.weixin.qq.com/wxa/get_auditstatus?access_token=%1%';


    /**
     * 查询最新一次提交的审核状态（仅供第三方代小程序调用）
     * @var string
     */
    public static $get_latest_auditstatus = 'https://api.weixin.qq.com/wxa/get_latest_auditstatus?access_token=%1%';

    /**
     *
     * 发布已通过审核的小程序（仅供第三方代小程序调用）
     *
     * @var string
     */
    public static $release = 'https://api.weixin.qq.com/wxa/release?access_token=%1%';

    /**
     *
     * 修改小程序线上代码的可见状态（仅供第三方代小程序调用）
     *
     * @var string
     */
    public static $change_visitstatus ='https://api.weixin.qq.com/wxa/change_visitstatus?access_token=%1%';


    /**
     * 小程序版本回退（仅供第三方代小程序调用）
     * @var string
     */
    public static $revertcoderelease = 'https://api.weixin.qq.com/wxa/revertcoderelease?access_token=%1%';

    /**
     * 查询当前设置的最低基础库版本及各版本用户占比 （仅供第三方代小程序调用）
     * @var string
     */
    public static $getweappsupportversion = 'https://api.weixin.qq.com/cgi-bin/wxopen/getweappsupportversion?access_token=%1%';

    /**
     * 设置最低基础库版本（仅供第三方代小程序调用）
     * @var string
     */
    public static $setweappsupportversion = 'https://api.weixin.qq.com/cgi-bin/wxopen/setweappsupportversion?access_token=%1%';


    /**
     * 设置小程序“扫普通链接二维码打开小程序”能力
     * 【增加或修改二维码规则】
     *
     * @var string
     */
    public static $qrcodejumpadd = 'https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpadd?access_token=%1%';

    /**
     * 设置小程序“扫普通链接二维码打开小程序”能力
     *  【获取已设置的二维码规则】
     * @var string
     */
    public static $qrcodejumpget = 'https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpget?access_token=%1%';


    /**
     * 设置小程序“扫普通链接二维码打开小程序”能力
     * 获取校验文件名称及内容
     * @var string
     */
    public static $qrcodejumpdownload = 'https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpdownload?access_token=%1%';

    /**
     * 设置小程序“扫普通链接二维码打开小程序”能力
     * 删除已设置的二维码规则
     * @var string
     */
    public static $qrcodejumpdelete = 'https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumpdelete?access_token=%1%';

    /**
     * 设置小程序“扫普通链接二维码打开小程序”能力
     * 发布已设置的二维码规则
     * @var string
     */
    public static $qrcodejumppublish = 'https://api.weixin.qq.com/cgi-bin/wxopen/qrcodejumppublish?access_token=%1%';

}