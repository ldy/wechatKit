<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/1/9
 * Time: 下午1:15
 */

namespace wechatkit\Mini;

use wechatkit\Config\Config;
use wechatkit\HttpFul\HttpFul;


/**
 *
 * 小程序域名设置
 *
 * Class Domain
 * @package wechatkit\Mini
 *
 */
class Domain{


    /**
     *
     * 设置小程序服务器域名
     *
     * @param $action
     *             // add添加, delete删除, set覆盖, get获取。当参数是get时不需要填四个域名字段。
     * @param $accessToken
     * @param null $domains
     * @return array|bool|null
     */
    function modifyDomain($action,$domains=null,$accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }


        $data = array(
            'action'    => $action
        );
        if($action != 'get' && !empty($domains)){
            $data['requestdomain'] = $domains['requestdomain'];
            $data['wsrequestdomain']   = $domains['wsrequestdomain'];
            $data['uploaddomain']   = $domains['uploaddomain'];
            $data['downloaddomain']   = $domains['downloaddomain'];
        }

        $response =  HttpFul::init()->handler(ApiConfig::$modifyDomain, [$accessToken], $data, 'JSON', 'POST');

        if($response && ($response['errcode'] == 0)){

            if('get' == $action){
                $domains = array(
                   'requestdomain'      =>  $response['requestdomain'],
                    'wsrequestdomain'   =>  $response['wsrequestdomain'],
                    'uploaddomain'      =>  $response['uploaddomain'],
                    'downloaddomain'    =>  $response['downloaddomain']
                );

                return $domains;
            }

            return true;
        }

        return false;
    }


    /**
     *
     * 授权给第三方的小程序，其业务域名只可以为第三方的服务器，当小程序通过第三方发布代码上线后，小程序原先自己配置的业务域名将被删除，只保留第三方平台的域名
     *
     * 提示：需要先将域名登记到第三方平台的小程序业务域名中，才可以调用接口进行配置
     *
     * @param $action
     * @param $accessToken
     * @param null $webviewdomain
     * @return array|bool
     */
    function setWebviewDomain($action,$webviewdomain=null,$accessToken=null){

        if(!$accessToken){
            $accessToken = Config::$accessToken;
        }

        $data = array(
            'action'    => $action
        );
        if($action != 'get' && !empty($domains)){
            $data['webviewdomain'] = $webviewdomain;
        }

        $response =  HttpFul::init()->handler(ApiConfig::$setwebviewDomain, [$accessToken], $data, 'JSON', 'POST');

        if($response && ($response['errcode'] == 0)){

            if('get' == $action){
                $domains = array(
                    'webviewdomain'  =>  $response['webviewdomain']
                );
                return $domains;
            }

            return true;
        }

        return false;
    }


}