<?php


namespace wechatkit\HttpFul;

use Httpful\Request;
use wechatkit\Config\Config;


class HttpFul implements \wechatkit\Core\HttpFul
{
    public $accessToken = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%1%&secret=%2%';
    public $jsApiTicket = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%1%&type=jsapi';

    public $oAuth2AccessTokenUserInfo = 'https://api.weixin.qq.com/sns/userinfo?access_token=%1%&openid=%2%&lang=zh_CN ';

    public $oauth2AccessToken = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%1%&secret=%2%&code=%3%&grant_type=authorization_code ';
    public $openOauth2AccessToken = 'https://api.weixin.qq.com/sns/oauth2/component/access_token?appid=%1%&code=%2%&grant_type=authorization_code&component_appid=%3%&component_access_token=%4%';
    public $apiComponentToken = 'https://api.weixin.qq.com/cgi-bin/component/api_component_token';
    public $apiAuthorizerToken = 'https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=%1%';

    public $preAuthCode = 'https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=%1%';
    public $apiQueryAuth = 'https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=%1%';
    public $apiGetAuthorizerInfo = 'https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token=%1%';
    public $uploadimg = 'https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%1%';
    public $shakearoundMaterialAdd = 'https://api.weixin.qq.com/shakearound/material/add?access_token=%1%&type=%2%';

    public $card = 'https://api.weixin.qq.com/card/create?access_token=%1%';
    public $applyid = 'https://api.weixin.qq.com/shakearound/device/applyid?access_token=%1%';
    public $applystatus = 'https://api.weixin.qq.com/shakearound/device/applystatus?access_token=%1%';
    public $deviceSearch = 'https://api.weixin.qq.com/shakearound/device/search?access_token=%1%';
    public $pageAdd = 'https://api.weixin.qq.com/shakearound/page/add?access_token=%1%';
    public $pageUpdate = 'https://api.weixin.qq.com/shakearound/page/update?access_token=%1%';
    public $deviceeBindpage = 'https://api.weixin.qq.com/shakearound/device/bindpage?access_token=%1%';
    public $getshakeinfo = 'https://api.weixin.qq.com/shakearound/user/getshakeinfo?access_token=%1%';
    public $promotion = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers';
    //公众号红包
    public $sendredpack = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack';

    public $getticketCard = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%1%&type=wx_card';
    public $unifiedorder = 'https://api.mch.weixin.qq.com/pay/unifiedorder';

    //小程序获取用户
    public $openOauth2JSAccessToken = 'https://api.weixin.qq.com/sns/jscode2session?appid=%1%&secret=%2%&js_code=%3%&grant_type=authorization_code';

    //小程序第三方授权获取登陆
    public $thirdOauth2JsAccessToken = 'https://api.weixin.qq.com/sns/component/jscode2session?appid=%1%&js_code=%2%&grant_type=authorization_code&component_appid=%3%&component_access_token=%4%';

    static public $self = NULL;

    static public function init()
    {
        if (NULL == self::$self) {
            self::$self =  new HttpFul();
        }
        return self::$self;
    }

    /**
     * @param $name
     * @param array $args
     * @param string $method
     * @return bool|mixed
     */
    public function handler($name, array $args, $body=[],$bodyType='JSON', $method = 'GET',$ca=[])
    {
        //if ($this->$name)
        {
            switch ($method){
                case 'GET':
                    return $ret = $this->httpGet($name, $args);
                    break;
                case 'POST':
                    if('JSON' == $bodyType){
                        $body = json_encode($body,JSON_UNESCAPED_UNICODE);
                        $body = str_replace(' ','',$body);
                    }
                    return $ret = $this->httpPost($name, $args,$body,$bodyType);
                    break;
                case 'FILE':
                    return $ret = $this->httpFile($name, $args,$body);
                    break;
                case 'CA':
                    return $ret = $this->httpCa($name,$body,$ca);
                    break;
            }

        }
    }


    /**
     * @param $urlName
     * @param $args
     * @return bool|mixed
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function httpGet($urlName, $args)
    {
        debug('----'.$urlName);
        $searchArr = [];
        for ($i = 1; $i <= count($args); $i++) {
            $searchArr[] = '%' . $i . '%';
        }
        if(startWith($urlName,'http')){
            $url = str_replace($searchArr, $args, $urlName);
        }else {
            $url = str_replace($searchArr, $args, $this->$urlName);
        }
        $response = Request::get($url)
            ->withStrictSSL()
            ->send();

        debug($url.'---'.$response);


        return $this->responseHandler($response, $urlName);
    }

    public function httpPost($name,$args,$body,$type){
        $searchArr = [];
        for ($i = 1; $i <= count($args); $i++) {
            $searchArr[] = '%' . $i . '%';
        }
        
        debug("------test---".$name);
        debug("------args---".json_encode($args));
        debug("------searchArr---".json_encode($searchArr));

        if(startWith($name,'http')){
            $url = str_replace($searchArr, $args, $name);
        }else {
            $url = str_replace($searchArr, $args, $this->$name);
        }

        $response = '';
        if($type == 'JSON'){

            $response = Request::post($url)
                ->withStrictSSL()
                ->expectsJson()
                ->body($body)
                ->send();
        }elseif($type == 'XML'){
            $response = Request::post($url)
                ->withStrictSSL()
                ->expectsXml()
                ->body($body)
                ->send();
        }

        return $this->responseHandler($response, $name);
    }

    public function httpFile($name,$args,$body){
        $searchArr = [];
        for ($i = 1; $i <= count($args); $i++) {
            $searchArr[] = '%' . $i . '%';
        }
        if(startWith($name,'http')){
            $url = str_replace($searchArr, $args, $name);
        }else {
            $url = str_replace($searchArr, $args, $this->$name);
        }

        $response = \Httpful\Request::post($url)
            ->withStrictSSL()
            ->attach($body)
            ->send();

        debug(json_encode($response));

        return $this->responseHandler($response, $name);
    }

    public function httpCa($name,$body,$ca){

        if(startWith($name,'http')){
            $url = str_replace('', '', $name);
        }else {
            $url = str_replace('', '', $this->$name);
        }
        $response = \Httpful\Request::post($url)
            ->authenticateWithCert($ca['cert_file'], $ca['key_file'], $ca['ca_file'])
            ->withStrictSSL()
            ->body($body)
            ->send();


        return $this->responseHandler($response, $name);
    }


    /**
     * @param $response
     * @param null $name
     * @return bool|mixed
     */
    public function responseHandler($response, $name = NULL)
    {
        $body = $response->raw_body;


        if($this->is_Json($response->raw_body)){
            $body = json_decode($response->raw_body,1);

        }else if($this->is_Xml($response->raw_body)){
            $body = json_decode(json_encode(simplexml_load_string($response->raw_body, 'SimpleXMLElement', LIBXML_NOCDATA)),1);
            debug($body);
        }else{
            return $body;
        }

        if (!isset($body['errcode']) || 0 == $body['errcode']) {
            if (NULL != $name) {
                return $this->filter($name, $body);
            } else {
                return $body;
            }
        } else {
            if(function_exists('debug')){
                debug("[{$name}][{$body['errcode']}]{$body['errmsg']}");
            }
            return FALSE;
        }
    }

    /**
     * @param $obj
     * @return mixed
     */
    public function objToArray($obj)
    {
        return json_decode(json_encode($obj), 1);
    }

    /**
     * @param $name
     * @param $resp
     * @return mixed
     */
    public function filter($name, $resp)
    {
        switch ($name) {
            case 'accessToken':
                return $resp['access_token'];
                break;

            case 'jsApiTicket':
                return $resp['ticket'];
                break;

            default:
                return $resp;
                break;
        }
    }

    public function is_Json($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function is_Xml($xml){
        $xml_parser = xml_parser_create();
        if(!xml_parse($xml_parser,$xml,true)){
            xml_parser_free($xml_parser);
            return false;
        }else {
            return true;
        }
    }
}
