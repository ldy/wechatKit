<?php
	namespace wechatkit\Message;

	class Message {

		public function replayText($content,$toUser,$formUser){
			$xml = "
				<xml>
				<ToUserName><![CDATA[%s]]></ToUserName>
				<FromUserName><![CDATA[%s]]></FromUserName>
				<CreateTime>".time()."</CreateTime>
				<MsgType><![CDATA[text]]></MsgType>
				<Content><![CDATA[%s]]></Content>
				</xml>
			";

			echo sprintf($xml,$toUser,$formUser,$content);
		}
	}