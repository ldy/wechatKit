Doc
2016-06-19

Wechat Api

INSTALL
    php composer require ldy/wechatkit -vvv
    
Start    
`
    *******************************************
    @param $type 
           接入类型 
                   O 开放平台
                   A 普通接入
          
    @param $options   根据自己需求填写
            [
            
             'appid'=>null,  
             'secret'=>null,
             
             'mch_id'=>null,
             'key'=>null,
             'cert_file'=>null,
             'key_file'=>null,
             
             'open'=>[
                      'appid'=>null,
                      'secret'=>null
                      ]
             ]
             
        
    @param $config   系统配置
            [
                'cachePath' => '/Tmp/HWeChatApi/%s', //%s 为APPID替代 此公众号下所有凭证都会缓存在此目录下
                'cacheTime' => '7000', //缓存有效时间
            ]


    $wechat = (new \wechatkit\Run\Run())->App($type, array $options=[],array $config=[]);
     
    *******************************************
    `
